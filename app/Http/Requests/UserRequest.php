<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'name' => ['required', 'string', 'max:255'],
                    'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                    'phone' => ['required', 'numeric', 'min:7'],
                    'password' => ['required', 'string', 'min:8', 'confirmed'],
                    'street' => ['required', 'string'],
                    'city' => ['required', 'string'],
                    'state' => ['required', 'string'],
                    'postal_code' => ['required', 'numeric'],
                    'country' => ['required', 'string'],
                ];
                break;
            case 'DELETE':
                return [];
                break;
            case 'PUT':
                return [
                    'name' => ['required', 'string', 'max:255'],
                    'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                    'phone' => ['required', 'numeric', 'min:7'],
                    'street' => ['required', 'string'],
                    'city' => ['required', 'string'],
                    'state' => ['required', 'string'],
                    'postal_code' => ['required', 'numeric'],
                    'country' => ['required', 'string'],
                ];
                break;
            default:
                return [];
                break;
        }
    }
}
