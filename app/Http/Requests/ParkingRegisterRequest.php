<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ParkingRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'user_id' => 'required|numeric|exists:User,id',
                ];
                break;
            case 'DELETE':
                return [];
                break;
            case 'PUT':
                return [];
                break;
            default:
                return [];
                break;
        }
    }

    public function messages()
    {
        return [
            'exists' => __('user not found'),
        ];
    }
}
