<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SeminarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'student_id' => 'nullable|numeric|exists:User,id',
                    'professor_id' => 'nullable|numeric|exists:User,id',
                    'start' => 'required|date',
                    'end' => 'required|date|after:start',
                ];
                break;
            case 'DELETE':
                return [];
                break;
            case 'PUT':
                return [
                    'student_id' => 'nullable|numeric|exists:User,id',
                    'professor_id' => 'nullable|numeric|exists:User,id',
                    'start' => 'required',
                    'end' => 'required',
                ];
                break;
            default:
                return [];
                break;
        }
    }
}
