<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\User;
use App\Models\Address;
use App\Models\Professor;
use App\Models\Student;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->middleware('auth');
        try {
            $users = User::all();
            if ($users == null) {
                return response(404, 'not found');
            }
            $users->address;
            $users->student;
            $users->professor;
            $users->parking;
            return response(200, $users);
        } catch (\Throwable $th) {
            throw $th;
            return response(500, 'Error interno');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $this->middleware('guest');
        try {
            $user = User::create([
                'name' => $request['name'],
                'email' => $request['email'],
                'password' => Hash::make($request['password']),
                'phone' => $request['phone'],
            ]);
            Address::create([
                'street' => $request['street'],
                'city' => $request['city'],
                'state' => $request['state'],
                'postal_code' => $request['postal_code'],
                'country' => $request['country'],
                'user_id' => $user->id,
            ]);

            if ($request['userType'] === '2') {
                Professor::create([
                    'user_id' => $user->id,
                ]);
            } else {
                Student::create([
                    'user_id' => $user->id,
                    'number_student' => null
                ]);
            }
            return response(202, $user);
        } catch (\Throwable $th) {
            throw $th;
            return response(500, 'Error interno');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->middleware('auth');
        try {
            $user = User::find($id);
            if ($user == null) {
                return response(404, 'not found');
            }
            $user->address;
            $user->student;
            $user->professor;
            $user->parking;
            return response(200, $user);
        } catch (\Throwable $th) {
            throw $th;
            return response(500, 'Error interno');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $this->middleware('auth');
        try {
            $user = User::find($id);
            if ($user == null) {
                return response(404, 'not found');
            }
            $user->update([
                'name' => $request['name'],
                'email' => $request['email'],
                'phone' => $request['phone'],
            ]);
            Address::where('user_id', $id)->update([
                'street' => $request['street'],
                'city' => $request['city'],
                'state' => $request['state'],
                'postal_code' => $request['postal_code'],
                'country' => $request['country'],
                'user_id' => $user->id,
            ]);

            if ($request['userType'] === '2') {
                Professor::create([
                    'user_id' => $user->id,
                ]);
                Student::where('user_id', $id)->delete();
            } else {
                Student::create([
                    'user_id' => $user->id,
                    'number_student' => null
                ]);
                Professor::where('user_id', $id)->delete();
            }
            return response(202, $user);
        } catch (\Throwable $th) {
            throw $th;
            return response(500, 'Error interno');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->middleware('auth');
        try {
            $user = User::find($id);
            if ($user == null) {
                return response(404, 'not found');
            }
            $response = $user->delete();
            return response(200, $response);
        } catch (\Throwable $th) {
            throw $th;
            return response(500, 'Error interno');
        }
    }
}
