<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\ParkingRegisterRequest;
use App\Models\ParkingRegister;

class ParkingRegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $parking_registers = ParkingRegister::all();
            if ($parking_registers == null) {
                return response(404, 'not found');
            }
            return response(200, $parking_registers);
        } catch (\Throwable $th) {
            throw $th;
            return response(500, 'Error interno');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ParkingRegisterRequest $request)
    {
        try {
            $parking_register = new ParkingRegister();
            $response = $parking_register->create($request->all());
            return response(202, $response);
        } catch (\Throwable $th) {
            throw $th;
            return response(500, 'Error interno');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $parking_register = ParkingRegister::find($id);
            if ($parking_register == null) {
                return response(404, 'not found');
            }
            return response(200, $parking_register);
        } catch (\Throwable $th) {
            throw $th;
            return response(500, 'Error interno');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ParkingRegisterRequest $request, $id)
    {
        try {
            $parking_register = ParkingRegister::find($id);
            if ($parking_register == null) {
                return response(404, 'not found');
            }
            $response = $parking_register->update($request->all());
            return response(202, $response);
        } catch (\Throwable $th) {
            throw $th;
            return response(500, 'Error interno');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $parking_register = ParkingRegister::find($id);
            if ($parking_register == null) {
                return response(404, 'not found');
            }
            $response = $parking_register->delete();
            return response(200, $response);
        } catch (\Throwable $th) {
            throw $th;
            return response(500, 'Error interno');
        }
    }
}
