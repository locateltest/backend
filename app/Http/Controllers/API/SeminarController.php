<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\SeminarRequest;
use App\Models\Seminar;

class SeminarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $seminars = Seminar::all();
            if ($seminars == null) {
                return response(404, 'not found');
            }
            return response(200, $seminars);
        } catch (\Throwable $th) {
            throw $th;
            return response(500, 'Error interno');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SeminarRequest $request)
    {
        try {
            $verify = Seminar::verifyAssingCalendar($request['student_id'], $request['professor_id'], $request['start'], $request['end']);
            if (!$verify['status']) {
                return response($verify['msg'], 422);
            }
            $seminar = new Seminar();
            $response = $seminar->create($request->all());
            return response(202, $response);
        } catch (\Throwable $th) {
            throw $th;
            return response(500, 'Error interno');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $seminar = Seminar::find($id);
            if ($seminar == null) {
                return response(404, 'not found');
            }
            return response(200, $seminar);
        } catch (\Throwable $th) {
            throw $th;
            return response(500, 'Error interno');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SeminarRequest $request, $id)
    {
        try {
            $verify = Seminar::verifyAssingCalendar($request['student_id'], $request['professor_id'], $request['start'], $request['end']);
            if (!$verify['status']) {
                return response($verify['msg'], 422);
            }
            $seminar = Seminar::find($id);
            if ($seminar == null) {
                return response(404, 'not found');
            }
            $response = $seminar->update($request->all());
            return response(202, $response);
        } catch (\Throwable $th) {
            throw $th;
            return response(500, 'Error interno');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $seminar = Seminar::find($id);
            if ($seminar == null) {
                return response(404, 'not found');
            }
            $response = $seminar->delete();
            return response(200, $response);
        } catch (\Throwable $th) {
            throw $th;
            return response(500, 'Error interno');
        }
    }
}
