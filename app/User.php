<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function address()
    {
        return $this->hasOne('App\Models\Address');
    }

    public function professor()
    {
        return $this->hasOne('App\Models\Professor');
    }

    public function student()
    {
        return $this->hasOne('App\Models\Student');
    }

    public function parking()
    {
        return $this->hasMany('App\Models\ParkingRegister');
    }

    public function seminarStudent()
    {
        return $this->hasMany('App\Models\Seminar', 'student_id');
    }

    public function seminarProfessor()
    {
        return $this->hasMany('App\Models\Seminar', 'professor_id');
    }

}
