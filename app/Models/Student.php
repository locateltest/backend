<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'number_student', 'average_mark', 'user_id'
    ];

    private $initalNumberStudent = 1000;

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function setNumberStudentAttribute($value)
    {
        if (isset($value)) {
            $this->attributes['number_student'] = $value;
        } else {
            $lastStudent = Student::orderBy('number_student', 'desc')->first();
            $this->attributes['number_student'] = $lastStudent != null ? $lastStudent->number_student + 1 : $this->initalNumberStudent;
        }
    }
}
