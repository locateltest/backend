<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seminar extends Model
{
    public function userStudent()
    {
        return $this->belongsTo('App\User', 'id', 'student_id');
    }

    public function userProfessor()
    {
        return $this->belongsTo('App\User', 'id', 'professor_id');
    }

    public function scopeVerifyAssingCalendar($query, $student_id, $professor_id, $start, $end)
    {
        $state = ['status' => true, 'msg' => ''];
        if (Seminar::where('student_id', $student_id)->whereBetween('start', [$start, $end])->orWhereBetween('end', [$start, $end])->first()) {
            $state['status'] = false;
            $state['msg'] = ['student_id' => 'The student has already occupied this space on the calendar'];
        }
        if (Seminar::where('professor_id', $professor_id)->whereBetween('start', [$start, $end])->orWhereBetween('end', [$start, $end])->first()) {
            $state['status'] = false;
            $state['msg'] = ['professor_id' => 'The professor has already occupied this space on the calendar'];
        }
        return $state;
    }
}
